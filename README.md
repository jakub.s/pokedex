# Pokedex

Project created using React.

<img src="pokedex.png" alt="pokedex" style="width:1000px;"/>

## Available Scripts

In the project directory, you can run:

### `npm install`

Downloads and install all required modules.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.
