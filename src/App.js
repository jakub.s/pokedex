import './css/App.css';
import Pokedex from './components/Pokedex';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Modal from './components/Modal';

const App = () => {
	return (
		<BrowserRouter>
			<div className="App">
				<Pokedex />
			</div>
			<Switch>
				<Route exact path="/pokemon/:id" children={<Modal />} />
			</Switch>
		</BrowserRouter>
	);
};

export default App;
