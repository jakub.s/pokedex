import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';

const Filters = ({ types, filters, setFilters, setFavouritesClicked }) => {
	return (
		<div className="App-filter">
			<div className="checkbox">
				<Checkbox onChange={(event, value) => setFavouritesClicked(value)} />
				<span>Favourites</span>
			</div>
			{types.map((type) => (
				<div className="checkbox" key={type}>
					<Checkbox
						color="primary"
						onChange={(event, value) => {
							if (value) {
								setFilters([...filters, type]);
							} else {
								let temp = [...filters];
								const filterIndex = filters.indexOf(type);
								temp.splice(filterIndex, 1);
								setFilters(temp);
							}
						}}
					/>
					<span>{type}</span>
				</div>
			))}
		</div>
	);
};
export default Filters;
