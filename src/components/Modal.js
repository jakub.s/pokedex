import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';

const Modal = () => {
	let history = useHistory();
	const { id } = useParams();
	const [details, setDetails] = useState({});
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	useEffect(() => {
		axios
			.get(`https://api.pokemontcg.io/v2/cards/${id}`)
			.then((response) => {
				if (response.status !== 200) {
					throw Error('Could not fetch the data.');
				} else {
					setError(null);
					setLoading(false);
					setDetails(response.data.data);
				}
			})
			.catch((error) => setError(error.message));
	}, [id]);

	const back = (event) => {
		event.stopPropagation();
		history.goBack();
	};

	if (error)
		return (
			<div className="Overlay" onClick={back}>
				<div className="Modal" onClick={(e) => e.stopPropagation()}>
					<div className="Modal-loading">
						<h2>{error}</h2>
					</div>
				</div>
			</div>
		);

	if (loading)
		return (
			<div className="Overlay" onClick={back}>
				<div className="Modal" onClick={(e) => e.stopPropagation()}>
					<div className="Modal-loading">
						<CircularProgress color="inherit" size={70} />
					</div>
				</div>
			</div>
		);

	return (
		<div className="Overlay" onClick={back}>
			<div className="Modal" onClick={(e) => e.stopPropagation()}>
				<div className="Details">
					<h1>Name: {details.name}</h1>
					<span>Types: </span>
					{details.types &&
						details.types.map((type) => <li key={type}>{type} </li>)}
					<br />
					<span>Subtypes: </span>
					{details.types &&
						details.subtypes.map((subtype) => (
							<span key={subtype}>{subtype} </span>
						))}
					<p>Rarity: {details.rarity}</p>
					<span>National Pokedex Numbers: </span>
					{details.nationalPokedexNumbers &&
						details.nationalPokedexNumbers.map((number) => (
							<li key={number}>{number} </li>
						))}
					<span>Attacks: </span>
					{details.attacks &&
						details.attacks.map((attack) => (
							<li key={attack.name}>{attack.name} </li>
						))}
					{details.evolvesFrom && <p>Evolves from: {details.evolvesFrom}</p>}
					{details.evolvesTo && <p>Evolves to: {details.evolvesTo}</p>}
				</div>
				{details.images && <img src={details.images.large} alt="card" />}
				<CloseIcon size="large" onClick={back} />
			</div>
		</div>
	);
};

export default Modal;
