import React, { useEffect, useRef, useState } from 'react';
import Button from '@material-ui/core/Button';
import Card from './Card';

const PokemonList = ({ currentCards, favourites, setFavourites }) => {
	const [page, setPage] = useState(1);
	const [displayingCards, setDisplayingCards] = useState([]);
	const prevCurrentCards = useRef(currentCards);

	useEffect(() => {
		setDisplayingCards(prevCurrentCards.current.slice(0, 10));
	}, []);

	useEffect(() => {
		if (prevCurrentCards.current.length !== currentCards.length) {
			setDisplayingCards(currentCards.slice(0, 10));
			setPage(1);
			prevCurrentCards.current = currentCards;
		}
	}, [currentCards]);

	return (
		<>
			<main className="App-main">
				{displayingCards.map((card) => (
					<Card
						key={card.id}
						card={card}
						favourites={favourites}
						setFavourites={setFavourites}
					/>
				))}
			</main>
			<Footer
				page={page}
				currentCards={currentCards}
				setPage={setPage}
				setDisplayingCards={setDisplayingCards}
			/>
		</>
	);
};

const Footer = ({ page, currentCards, setPage, setDisplayingCards }) => {
	if (page * 10 > currentCards.length) return null;
	return (
		<footer className="App-footer">
			<Button
				variant="contained"
				size="large"
				onClick={() => {
					setPage((prev) => prev + 1);
					setDisplayingCards((prev) => [
						...prev,
						...currentCards.slice(page * 10, page * 10 + 10),
					]);
				}}
			>
				Load more
			</Button>
		</footer>
	);
};

export default PokemonList;
