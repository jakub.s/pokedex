import { useEffect, useState } from 'react';
import axios from 'axios';
import ClipLoader from 'react-spinners/HashLoader';
import PokemonList from './PokemonList';
import SearchBar from './SearchBar';
import Filters from './Filters';
import useFilter from '../hooks/useFilter';

const Pokedex = () => {
	const [allCards, setAllCards] = useState([]);
	const [currentCards, setCurrentCards] = useState([]);
	const [types, setTypes] = useState([]);
	const [favourites, setFavourites] = useState([]);
	const [filters, setFilters] = useState([]);

	const [loading, setLoading] = useState(true);
	const [searchTerm, setSearchTerm] = useState('');
	const [favouritesClicked, setFavouritesClicked] = useState(false);
	const [error, setError] = useState(null);

	useEffect(() => {
		axios
			.get('https://api.pokemontcg.io/v2/cards')
			.then((response) => {
				if (response.status !== 200) {
					throw Error('Could not fetch the data.');
				} else {
					setError(null);
					setAllCards(response.data.data);
					setCurrentCards(response.data.data);
					setLoading(false);
				}
			})
			.catch((error) => setError(error.message));

		axios
			.get('https://api.pokemontcg.io/v2/types')
			.then((response) => {
				if (response.status !== 200) {
					throw Error('Could not fetch types.');
				} else {
					setTypes(response.data.data);
				}
			})
			.catch((error) => console.log(error));
	}, []);

	useFilter(
		filters,
		favouritesClicked,
		favourites,
		allCards,
		searchTerm,
		setCurrentCards
	);

	if (error)
		return (
			<div className="Loading">
				<h2>{error}</h2>
			</div>
		);

	if (loading)
		return (
			<div className="Loading">
				<ClipLoader color="white" loading={loading} size={200} />
			</div>
		);

	return (
		<>
			<SearchBar setSearchTerm={setSearchTerm} />
			<Filters
				types={types}
				filters={filters}
				setFilters={setFilters}
				setFavouritesClicked={setFavouritesClicked}
			/>
			<PokemonList
				currentCards={currentCards}
				favourites={favourites}
				setFavourites={setFavourites}
			/>
		</>
	);
};

export default Pokedex;
