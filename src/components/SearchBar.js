import Input from '@material-ui/core/Input';

const SearchBar = ({ setSearchTerm }) => {
	return (
		<header className="App-header">
			<Input
				aria-label="search-input"
				className="Search-input"
				placeholder="Search for pokemon, type or subtype..."
				onChange={(event) => setSearchTerm(event.target.value)}
			/>
		</header>
	);
};
export default SearchBar;
