import React from 'react';
import { Link } from 'react-router-dom';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Rating from '@material-ui/lab/Rating';
import { withStyles } from '@material-ui/core/styles';

const StyledRating = withStyles({
	iconFilled: {
		color: '#ff6d75',
	},
	iconHover: {
		color: '#ff3d47',
	},
})(Rating);

const Card = ({ card, favourites, setFavourites }) => {
	const defaultHeartValue = (card) => (favourites.indexOf(card) === -1 ? 0 : 1);

	const handleFavouriteClick = (event, value) => {
		if (value) {
			setFavourites((prev) => [...prev, card]);
		} else {
			setFavourites((prev) => prev.filter((elem) => elem.id !== card.id));
		}
	};

	return (
		<div className="card">
			<Link
				to={{
					pathname: `/pokemon/${card.id}`,
				}}
			>
				<img src={card.images.large} alt="card" />
			</Link>
			<p>{card.name}</p>
			<StyledRating
				name={card.id}
				value={defaultHeartValue(card)}
				max={1}
				size="large"
				onChange={handleFavouriteClick}
				icon={<FavoriteIcon fontSize="inherit" />}
			/>
		</div>
	);
};

export default Card;
