import { useEffect } from 'react';

const useFilter = (
	filters,
	favouritesClicked,
	favourites,
	allCards,
	searchTerm,
	setCurrentCards
) => {
	useEffect(() => {
		if (!searchTerm) {
			if (favouritesClicked) {
				if (filters.length === 0) {
					setCurrentCards(favourites);
				} else {
					const res = favourites.reduce((acc, curr) => {
						if (curr.types.some((a) => filters.some((b) => b === a))) {
							return [...acc, curr];
						} else {
							return acc;
						}
					}, []);
					setCurrentCards(res);
				}
			} else {
				if (filters.length === 0) {
					setCurrentCards(allCards);
				} else {
					const res = allCards.reduce((acc, curr) => {
						if (curr.types.some((a) => filters.some((b) => b === a))) {
							return [...acc, curr];
						} else {
							return acc;
						}
					}, []);
					setCurrentCards(res);
				}
			}
		} else {
			if (favouritesClicked) {
				if (filters.length === 0) {
					setCurrentCards(
						favourites.filter(
							(card) =>
								card.name
									.toLocaleLowerCase()
									.includes(searchTerm.toLocaleLowerCase()) ||
								card.types.some((type) =>
									type
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								) ||
								card.subtypes.some((subtype) =>
									subtype
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								)
						)
					);
				} else {
					const res = favourites.reduce((acc, curr) => {
						if (curr.types.some((a) => filters.some((b) => b === a))) {
							return [...acc, curr];
						} else {
							return acc;
						}
					}, []);
					setCurrentCards(
						res.filter(
							(card) =>
								card.name
									.toLocaleLowerCase()
									.includes(searchTerm.toLocaleLowerCase()) ||
								card.types.some((type) =>
									type
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								) ||
								card.subtypes.some((subtype) =>
									subtype
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								)
						)
					);
				}
			} else {
				if (filters.length === 0) {
					setCurrentCards(
						allCards.filter(
							(card) =>
								card.name
									.toLocaleLowerCase()
									.includes(searchTerm.toLocaleLowerCase()) ||
								card.types.some((type) =>
									type
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								) ||
								card.subtypes.some((subtype) =>
									subtype
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								)
						)
					);
				} else {
					const res = allCards.reduce((acc, curr) => {
						if (curr.types.some((a) => filters.some((b) => b === a))) {
							return [...acc, curr];
						} else {
							return acc;
						}
					}, []);
					setCurrentCards(
						res.filter(
							(card) =>
								card.name
									.toLocaleLowerCase()
									.includes(searchTerm.toLocaleLowerCase()) ||
								card.types.some((type) =>
									type
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								) ||
								card.subtypes.some((subtype) =>
									subtype
										.toLocaleLowerCase()
										.includes(searchTerm.toLocaleLowerCase())
								)
						)
					);
				}
			}
		}
	}, [
		filters,
		favouritesClicked,
		favourites,
		allCards,
		searchTerm,
		setCurrentCards,
	]);
};

export default useFilter;
