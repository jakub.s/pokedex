import { render } from '@testing-library/react';
import SearchBar from './components/SearchBar';
import '@testing-library/jest-dom/extend-expect';

test('renders SearchBar component', async () => {
	const { queryByPlaceholderText } = render(<SearchBar />);
	expect(
		queryByPlaceholderText('Search for pokemon, type or subtype...')
	).toBeTruthy();
});
